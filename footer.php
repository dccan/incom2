<!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="add" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              <h5>Personal-info</h5>
            </div>
            <div class="widget-content nopadding">
              <form id="addFrom" action="/pages/sanpham.php" method="post" class="form-horizontal">
                
                <!-- <div class="control-group">
                  <label class="control-label">ten_sp Name :</label>
                  <div class="controls">
                    <input name="ma_sp" type="hidden" class="span11" 
                    value="null" 
                    >
                  </div>
                </div> -->

                <div class="control-group">
                  <label class="control-label">ten_sp Name :</label>
                  <div class="controls">
                    <input name="ten_sp" type="text" class="span11" placeholder="First name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">hinh Name :</label>
                  <div class="controls">
                    <input name="hinh" type="text" class="span11" placeholder="First name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">mo_ta Name :</label>
                  <div class="controls">
                    <input name="mo_ta" type="text" class="span11" placeholder="Last name">
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">so_luong info :</label>
                  <div class="controls">
                    <input name="so_luong" type="int" class="span11" placeholder="Company name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">don_gia info :</label>
                  <div class="controls">
                    <input name="don_gia" type="int" class="span11" placeholder="Company name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">don_vi_tinh info :</label>
                  <div class="controls">
                    <input name="don_vi_tinh" type="int" class="span11" placeholder="Company name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">chi_tiet info :</label>
                  <div class="controls">
                    <input name="chi_tiet" type="int" class="span11" placeholder="Company name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">ma_loai info :</label>
                  <div class="controls">
                    <input name="ma_loai" type="int" class="span11" placeholder="Company name">
                  </div>
                </div>
                
                
                <div class="form-actions">
                  <button type="submit" name="addSubmit" class="btn btn-success">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


<!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <!-- <script src="vendor/chart.js/Chart.min.js"></script> -->
  <script src="vendor/datatables/jquery.dataTables.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="js/demo/datatables-demo.js"></script>
  <!-- <script src="js/demo/chart-area-demo.js"></script> -->

  <!-- <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script> -->
  <script src="ckfinder/ckfinder.js"></script>
  <!-- <script src="js/select2.min.js"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js" type="text/javascript" charset="utf-8" async defer></script> -->


  
  <script src="js/bootstrap-select.min.js"></script>
  
</body>

</html>
