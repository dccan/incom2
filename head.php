<!DOCTYPE html>
<html lang="en">

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <base href="http://localhost:8888/" />
  <title>INCOM</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin.min.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <!-- <link href="css/select2.min.css" rel="stylesheet"> -->

  <link href="css/print.css" rel="stylesheet" media="print">

  <link rel="stylesheet" href="css/bootstrap-select.min.css">
  
</head>

<body id="page-top">
<div id="wait">
  <img src='images/demo_wait.gif' width="64" height="64" />

</div>
  