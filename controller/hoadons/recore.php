<?php 
require_once('../DB_driver.php');
$db = new DB_driver();
$tenSP = $db->get_list("select ma_sp,ten_sp,don_gia from sanpham ");
?>

<tr class="copy">
<!--  <td>
  <input name="so_hd[]" class="form-control" type="number">
</td> -->
<td class="td-masp">
  <select class="selectpicker" name="ma_sp[]"  data-live-search="true" required>
    <option value="">Chọn sản phẩm</option>
    <?php
      foreach ($tenSP as $key => $value) {
    ?>
    <option gia="<?=$value['don_gia']?>" value="<?=$value['ma_sp']?>"><?=$value['ten_sp']?></option>
    <?php } ?>
  </select>

  <!-- <input class="ma_sp" name="ma_sp[]" class="form-control" type="text" required  ></input> -->
</td>
<td>
  <input name="so_luong[]" value="1" min='1' class="form-control so_luong" type="number" required  >
</td>
<td>
  <input  name="don_gia[]" value="0" class="form-control don_gia" type="number" readonly required >
</td>
<td class="td_thanh_tien">
  <input name="thanh_tien[]" value="0" class="form-control thanh_tien" type="number" required readonly >
</td>
<td>
  <input  name="khuyen_mai[]" min="0" value="0" class="form-control khuyen_mai" type="number"   />
</td>
<td>
  <input  class="tang form-control" name="tang[]" type="checkbox"  value="true">
  <input class="tang_hidden" type="hidden" name="tang[]" value="false">
  
</td>
<td>
  <a class="btn btn-danger rm_recore"><i class="fas fa-times"></i></a>
</td>
</tr>


<script>
  $('.selectpicker').selectpicker();

  $("#addFrom .copy .td-masp select" ).change(function() {
    position = $(this);
    don_gia = $('option:selected', this).attr('gia');
    
    // $('.copy td .don_gia').val(gia);
    $(position).parent().parent().parent().find('.don_gia').val(don_gia);
    so_luong = $(position).parent().parent().parent().find('.so_luong').val();
    
    khuyen_mai = $(position).parent().parent().parent().find('.khuyen_mai').val();
    
    if(khuyen_mai < 101) {
      thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
    }else {
      thanh_tien = don_gia * so_luong - khuyen_mai;  
    }  
    
    $(position).parent().parent().parent().find('.thanh_tien').val(thanh_tien);

  });

  $("#addFrom .copy td .so_luong").bind('keyup mouseup', function () {
    position = $(this);
    
    // so_luong = position.val(); 
    so_luong = $(position).parent().parent().find('.so_luong').val();
    don_gia = $(position).parent().parent().find('.don_gia').val();
    khuyen_mai = $(position).parent().parent().find('.khuyen_mai').val();
    
    if(khuyen_mai < 101) {
      thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
    }else {
      thanh_tien = don_gia * so_luong - khuyen_mai;  
    }  

    $(position).parent().parent().find('.thanh_tien').val(thanh_tien);   
       
  });



  $("#edit_form .copy .td-masp select" ).change(function() {
    position = $(this);
    don_gia = $('option:selected', this).attr('gia');
    
    // $('.copy td .don_gia').val(gia);
    $(position).parent().parent().parent().find('.don_gia').val(don_gia);
    so_luong = $(position).parent().parent().parent().find('.so_luong').val();
    
    khuyen_mai = $(position).parent().parent().parent().find('.khuyen_mai').val();
    
    tang = isset_tang_select(position);

    if( tang == false ) {
      if(khuyen_mai < 101) {
        thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
      }else {
        thanh_tien = don_gia * so_luong - khuyen_mai;  
      }  
    }else {
      thanh_tien = 0;
    }
    
    $(position).parent().parent().find('.thanh_tien').val(thanh_tien);
    tongtien();

  });


  $("#edit_form .copy td .so_luong").bind('keyup mouseup', function () {
    position = $(this);
    
    // so_luong = position.val(); 
    so_luong = $(position).parent().parent().find('.so_luong').val();
    don_gia = $(position).parent().parent().find('.don_gia').val();
    khuyen_mai = $(position).parent().parent().find('.khuyen_mai').val();
    thanh_tien = don_gia * so_luong;

    tang = isset_tang(position);
    if( tang == false ) {
      if(khuyen_mai < 101) {
        thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
      }else {
        thanh_tien = don_gia * so_luong - khuyen_mai;  
      }  
    }else {
      thanh_tien = 0;
    }

    $(position).parent().parent().find('.thanh_tien').val(thanh_tien);   
    tongtien();        
  });

  $("#addFrom .copy td .tang" ).change(function() {
    positin = $( this );
    if(positin.is( ":checked" ) == true) {
      positin.parent().find('.tang_hidden').prop('disabled', true);
    }else {
      positin.parent().find('.tang_hidden').prop('disabled', false);
    }
  });

  $("#edit_form .copy td .tang" ).change(function() {
    position = $( this );
    // alert(position.is( ":checked" ));
    if(position.is( ":checked" ) === true) {
      position.parent().find('.tang_hidden').prop('disabled', true);
    }else {
      position.parent().find('.tang_hidden').prop('disabled', false);
    }

  });

  $("#edit_form .copy td .khuyen_mai").bind('keyup mouseup', function () {
    position = $(this);

    so_luong = $(position).parent().parent().find('.so_luong').val();
    don_gia = $(position).parent().parent().find('.don_gia').val();
    
    khuyen_mai = $(position).parent().parent().find('.khuyen_mai').val();

    tang = isset_tang(position);
    
    khuyen_mai = $(position).parent().parent().find('.khuyen_mai').val();

    if( tang == false ) {
      if(khuyen_mai < 101) {
        thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
      }else {
        thanh_tien = don_gia * so_luong - khuyen_mai;  
      }  
    }else {
      thanh_tien = 0;
    }

    $(position).parent().parent().find('.thanh_tien').val(thanh_tien);   
    tongtien();        
  });

   $("#edit_form .copy td .tang ").change(function() {
    position = $(this);
    
    tang = change_tang(position);
    so_luong = $(position).parent().parent().find('.so_luong').val();
    don_gia = $(position).parent().parent().find('.don_gia').val();
    khuyen_mai = $(position).parent().parent().find('.khuyen_mai').val();
    
    if( tang == false ) {

      if(khuyen_mai < 101) {
        thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
      }else {
        thanh_tien = don_gia * so_luong - khuyen_mai;  
      }  
    }else {
      thanh_tien = 0;
    }

    $(position).parent().parent().find('.thanh_tien').val(thanh_tien);   
    tongtien();

  });

</script>