<?php
require_once('../DB_driver.php');
$db = new DB_driver();

// var_dump(($_POST));
// $db->insert('sanpham',$_POST);
if(isset($_GET['so_hd']))
  $idHD = $_GET['so_hd']; 

$hoadon = $db->get_list("select hoadon.*,khachhang.ho_ten as ho_ten, khachhang.ma_kh as ma_kh from hoadon,khachhang where khachhang.ma_kh = hoadon.ma_kh and so_hd =$idHD" );

$chiTietHD = $db->get_list("select * from chitiethoadon where so_hd=$idHD" );
$khachHang = $db->get_list('select ma_kh,ho_ten from khachhang');

$sanpham = $db->get_list("select ma_sp,ten_sp,don_gia from sanpham ");

?>

<form id="edit_form" action="" method="post"  >
    <div style="text-align: center; font-size: 20px;font-weight: bold; margin-bottom: 10px" class="title_print hidden ">Hóa Đơn</div>
    <table class="col-12 ">
      <tbody>
        <tr class="hidden">
          <td></td>
          
        </tr>
        <tr>
          <td >
            <label class="control-label">Số hóa đơn </label>
            <input name="so_hd" type="text" class="form-control" placeholder="Số hóa đơn auto"  readonly="readonly" value="<?=$hoadon[0]['so_hd']?>">
            <!-- <div class="controls">
              <input name="so_hd" type="hidden" class="form-control" placeholder="Số hóa đơn auto"  readonly="readonly">
            </div> -->
          </td>
          <td >
            <label class="control-label">Ngày lập</label>
            <input name="ngay_lap" type="text" class="form-control" placeholder=""  readonly="readonly" value="<?=$hoadon[0]['ngay_lap']?>">

          </td>
        </tr>

        <tr>
          <td >
            <label class="control-label">
              Khách hàng
            </label>
            <select  class="selectpicker form-control" name="ma_kh" data-show-subtext="true" data-live-search="true">
              <?php
                foreach ($khachHang as $key => $value) {
              ?>
                <option <?= ($hoadon[0]['ma_kh'] === $value['ma_kh'] ) ?'selected':''   ?>  value="<?=$value['ma_kh']?>" >
                  <?=$value['ho_ten']?> 
                </option>
              <?php } ?>
            </select>
            
          </td>
          <td >
            <label class="control-label">Trị giá: </label>
            <input  name="tri_gia" type="text" class="form-control tri_gia" placeholder=""  readonly="readonly" value="<?=$hoadon[0]['tri_gia']?>">
            <a><i class="fas fa-load"></i></a>
          </td>
        </tr>

        
        <tr>
          <td >
            <label class="control-label">Thanh toán:</label>
            <input name="thanh_toan" type="text" class="form-control" placeholder=""   value="<?=$hoadon[0]['thanh_toan']?>">
          </td>
          <td >
            <label class="control-label">Tình trạng: </label>
            <input name="tinh_trang" type="text" class="form-control" placeholder=""  value="<?=$hoadon[0]['tinh_trang']?>">

          </td>
        </tr>

      </tbody>
    </table>
    <table class="col-12 ">
      <thead>
        <tr>
          <!-- <th>Số hóa đơn</th> -->
          <th>Sản phẩm</th>
          <th>Số lượng</th>
          <th>Đơn giá</th>
          <th>Thành tiền</th>
          <th>Khuyến mãi</th>
          <th>Tặng</th>
          <th><a id="edit_recore" class="btn btn-success"><i class="fas fa-plus"></i></a></th>

        </tr>
      </thead>
      <tbody id="tbody-edit" >
        <?php
          foreach ($chiTietHD as $k => $v) {
        ?>
          <tr class="tr-sp">
            <td class="td-masp">
              <input type="hidden" value="<?=$v['id']?>" name="id[]" />
              <select class="selectpicker" name="ma_sp[]" data-show-subtext="true" data-live-search="true" required>
                <?php
                  foreach ($sanpham as $key => $value) {
                ?>
                  <option <?= ($v['ma_sp'] === $value['ma_sp']) ? 'selected' :'' ?>  gia="<?=$value['don_gia']?>" value="<?=$value['ma_sp']?>">
                    <?=$value['ten_sp']?> 
                  </option>
                <?php } ?>
              </select> 
            </td>
            <td>
              <input name="so_luong[]" class="form-control so_luong" min="1" type="number" value="<?=$v['so_luong']?>" />
            </td>
            <td>
              <input name="don_gia[]" class="form-control don_gia" min="1" readonly type="number" value="<?=$v['don_gia']?>" />
            </td>
            <td>
              <input name="thanh_tien[]" class="form-control thanh_tien" readonly min="0" type="number" value="<?=$v['thanh_tien']?>" />
            </td>
            <td>
              <input name="khuyen_mai[]" class="form-control khuyen_mai" min="0" type="number" value="<?=$v['khuyen_mai']?>" />
            </td>
            <td>
              
              <input <?=($v['tang'] === 'true')?"disabled":'' ?> class="tang_hidden" name="tang[]" type="hidden" value="false">

              <input name="tang[]" class="form-control tang" <?=($v['tang'] === 'true')?"checked=checked":'' ?> type="checkbox"  value="true" />

            </td>
            <td></td>
          </tr>
        <?php    
          }
        ?>
      </tbody>
    </table>
    <div class="form-actions">
      <button name="saveView" class="btn btn-primary d-print-none">
        Save
      </button>
      
      <!-- 
      <a data-toggle="modal" data-target="" class="btn btn-info d-print-none" id="back">
        Back
      </a>
      <a class="btn btn-warning d-print-none" id="print" >
        Print
      </a> -->
    </div>
  </form>

<script>
  $('.selectpicker').selectpicker();
  $('#edit_recore').click(function(e) {
    // Add recore
    
    $('#wait').show();
    url = 'controller/hoadons/recore.php';
    e.preventDefault(); 
    $.ajax({
        url: url,
        type: 'GET',
        success: function(data) {
            $('#wait').hide();
            // location.reload();
            $("#tbody-edit").append(data);
        },
        error: function(data){
            $('#wait').hide();
        }               
    });
  });

  $("#tbody-edit").on("click", ".rm_recore", function (event) {
        
    // Delete recore
    $(this).parent().parent('.copy').remove();
    tongtien();
  });

  $( "#edit_form .tr-sp td select" ).change(function() {
    
    position = $(this);

    don_gia = $('option:selected', this).attr('gia');
    
    $(position).parent().parent().parent().find('td .don_gia').val(don_gia);

    so_luong = $(position).parent().parent().parent().find('.so_luong').val();
    
    khuyen_mai = $(position).parent().parent().parent().find('.khuyen_mai').val();

    tang = isset_tang_select(position);
    if( tang == false ) {
      if(khuyen_mai < 101) {
        thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
      }else {
        thanh_tien = don_gia * so_luong - khuyen_mai;  
      }  
    }else {
      thanh_tien = 0;
    }

    $(position).parent().parent().find('.thanh_tien').val(thanh_tien);
    tongtien();

  });

  
  $("#edit_form .tr-sp td .so_luong").bind('keyup mouseup', function () {
    position = $(this);

    so_luong = $(position).parent().parent().find('.so_luong').val();
    don_gia = $(position).parent().parent().find('.don_gia').val();
    
    khuyen_mai = $(position).parent().parent().find('.khuyen_mai').val();

    tang = isset_tang(position);
    
    khuyen_mai = $(position).parent().parent().parent().find('.khuyen_mai').val();

    if( tang == false ) {
      if(khuyen_mai < 101) {
        thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
      }else {
        thanh_tien = don_gia * so_luong - khuyen_mai;  
      }  
    }else {
      thanh_tien = 0;
    }

    $(position).parent().parent().find('.thanh_tien').val(thanh_tien);   
    tongtien();        
  });

  $("#edit_form .tr-sp td .khuyen_mai").bind('keyup mouseup', function () {
    position = $(this);

    so_luong = $(position).parent().parent().find('.so_luong').val();

    
    don_gia = $(position).parent().parent().find('.don_gia').val();
    
    khuyen_mai = $(position).parent().parent().find('.khuyen_mai').val();
    
    tang = isset_tang(position);

    khuyen_mai = $(position).parent().parent().find('.khuyen_mai').val();

    if( tang == false ) {
      if(khuyen_mai < 101) {
        thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
      }else {
        thanh_tien = don_gia * so_luong - khuyen_mai;  
      }  
    }else {
      thanh_tien = 0;
    }

    $(position).parent().parent().find('.thanh_tien').val(thanh_tien);   
    tongtien();        
  });

  $("#edit_form .tr-sp td .tang ").change(function() {
    position = $(this);
    
    tang = change_tang(position);
    so_luong = $(position).parent().parent().find('.so_luong').val();
    don_gia = $(position).parent().parent().find('.don_gia').val();
    khuyen_mai = $(position).parent().parent().find('.khuyen_mai').val();
    
    if( tang == false ) {

      if(khuyen_mai < 101) {
        thanh_tien = don_gia * so_luong - ( don_gia * so_luong * khuyen_mai/100) ;  
      }else {
        thanh_tien = don_gia * so_luong - khuyen_mai;  
      }  
    }else {
      thanh_tien = 0;
    }

    $(position).parent().parent().find('.thanh_tien').val(thanh_tien);   
    tongtien();

  });

  $('#edit_form').submit(function(e) {

    $('#wait').show();
    dataForm = $('#edit_form').serialize();
    url = 'controller/hoadons/update.php';

    // dataJson = JSON.stringify(dataForm);
    console.log(dataForm);
    e.preventDefault(); 
    $.ajax({
        url: url,
        type: 'POST',
        data: dataForm,
        success: function(data) {
            $('#wait').hide();
            location.reload();
        },
        error: function(data){
            $('#wait').hide();
        }               
    });
  });

  $("#edit_form tr td .tang" ).change(function() {
    position = $( this );
    // alert(position.is( ":checked" ));
    if(position.is( ":checked" ) === true) {
      position.parent().find('.tang_hidden').prop('disabled', true);
    }else {
      position.parent().find('.tang_hidden').prop('disabled', false);
    }

  });


  function tongtien() {
    tong = 0;
    $("#edit_form .thanh_tien").each(function() {
        tong = tong + $(this).val() *1;
    });
    $('#edit_form .tri_gia').val(tong);
  }

  function change(){

  }

  function isset_tang(position) {
    return  $(position).parent().parent().find('.tang').change().prop( "checked" );
  }

  function isset_tang_select(position) {
    return  $(position).parent().parent().parent().find('.tang').change().prop( "checked" );
  }

  function change_tang(position) {
    return  $(position).prop( "checked" );
  }

</script>