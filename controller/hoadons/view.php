<?php
require_once('../DB_driver.php');
$db = new DB_driver();

// var_dump(($_POST));
// $db->insert('sanpham',$_POST);

$khachHang = $db->get_list('select ma_kh,ho_ten from khachhang  ');
$sanPham = $db->get_list("select ma_sp,ten_sp from sanpham ");

$dataHD = ['ngay_lap'=> $_POST['ngay_lap'], 'ma_kh'=> $_POST['ma_kh'], 'tri_gia'=> $_POST['tri_gia'], 'thanh_toan'=> $_POST['thanh_toan'], 'tinh_trang'=> $_POST['tinh_trang']  ];

$ma_sp = $_POST['ma_sp'];
$so_luong = $_POST['so_luong'];
$don_gia = $_POST['don_gia'];
$thanh_tien = $_POST['thanh_tien'];
$khuyen_mai = $_POST['khuyen_mai'];

$tang = $_POST['tang'];


// var_dump($tang);
// var_dump($khuyen_mai);


$count = count($ma_sp);


// if( $_POST['ngay_lap'] =='' || $_POST['ma_kh'] =='' || $_POST['thanh_toan'] =='' ||  $_POST['tinh_trang'] =='' ) {
  
//   echo "ERROR";
  
// }


$getIdHD = $db ->get_list('select so_hd from hoadon order BY so_hd DESC limit 1  ');

$idHD = $getIdHD[0]['so_hd'];
$tongTien = 0;

for($i=0; $i < $count; ++$i ) {
  
  if($tang[$i] == 'false')	{
    if( $khuyen_mai[$i] < 101 ) {
      $thanh_tien[$i] = $thanh_tien[$i] - ($thanh_tien[$i] * $khuyen_mai[$i]/100);
      $tongTien += $thanh_tien[$i];      
    }else {
      $thanh_tien[$i] = $thanh_tien[$i] - $khuyen_mai[$i];
      $tongTien += $thanh_tien[$i];      
    }
  }
           
}



?>

<div class="col-md-12 view_div">

  <form id="view_form" action="" method="post"  >
    <div style="text-align: center; font-size: 20px;font-weight: bold; margin-bottom: 10px" class="title_print hidden ">Hóa Đơn</div>
  	<table class="col-12">
  		<tbody>
  			<tr class="hidden">
  				<td></td>
  				
  			</tr>
  			<tr>
  				<td >
  					<label class="control-label">Số hóa đơn: <?=$idHD+1?> </label>
            <input name="so_hd" type="hidden" class="form-control" placeholder="Số hóa đơn auto"  readonly="readonly" value="<?=$_POST['so_hd']?>">
            <!-- <div class="controls">
              <input name="so_hd" type="hidden" class="form-control" placeholder="Số hóa đơn auto"  readonly="readonly">
            </div> -->
          </td>
  				<td >
  					<label class="control-label">Ngày lập: <?=$_POST['ngay_lap']?></label>
            <input name="ngay_lap" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="<?=$_POST['ngay_lap']?>">

  				</td>
  			</tr>

  			<tr>
  				<td >
  					<label class="control-label">Tên khách hàng: 
              <?php
                foreach ($khachHang as $key => $value) {
                    if($value['ma_kh'] === $_POST['ma_kh'])
                      echo $value['ho_ten'];
                }
              ?> 
            </label>
            <input name="ma_kh" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="<?=$_POST['ma_kh']?>">
            
          </td>
  				<td >
  					<label class="control-label">Trị giá: <?=number_format($tongTien)?></label>
            <input name="tri_gia" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="<?=$tongTien?>">
  				</td>
  			</tr>

        
  			<tr>
  				<td >
  					<label class="control-label">Thanh toán: <?=number_format($_POST['thanh_toan'])?></label>
            <input name="thanh_toan" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="<?=$_POST['thanh_toan']?>">
          </td>
          <td >
          	<label class="control-label">Tình trạng: <?=number_format($_POST['tinh_trang'])?> </label>
            <input name="tinh_trang" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="<?=$_POST['tinh_trang']?>">

          </td>
  			</tr>

  		</tbody>
  	</table>
  	<table class="table table-bordered dataTable">
      <thead>
        <tr>
          <!-- <th>Số hóa đơn</th> -->
          <th>Sản phẩm</th>
          <th>Số lượng</th>
          <th>Đơn giá</th>
          <th>Thành tiền</th>
          <th>Khuyến mãi</th>
          <th>Tặng</th>
        </tr>
      </thead>
      <tbody>
      	<?php
      		for($i=0; $i < $count; $i++ ) {
  				$dataCTHD =  [ 'so_hd' => $idHD , 'ma_sp' => $ma_sp[$i], 'so_luong' => $so_luong[$i], 'don_gia' => $don_gia[$i], 'thanh_tien' => $thanh_tien[$i] , 'khuyen_mai' => $khuyen_mai[$i] , 'tang' => $tang[$i] ];

          
  				
      	?>
  	      <tr >
  	        <td >
  	          <?php
                foreach ($sanPham as $key => $value) {
                    if($value['ma_sp'] === $ma_sp[$i] )
                      echo $value['ten_sp'];
                }
              ?> 
              

              <input name="ma_sp[]" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="<?=$ma_sp[$i]?>">
  	        </td>
  	        <td >
  	          <?=$so_luong[$i]?>
              <input name="so_luong[]" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="<?=$so_luong[$i]?>">
  	        </td>
  	        <td >
  	        	<?=number_format($don_gia[$i])?>
              <input name="don_gia[]" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="<?=$don_gia[$i]?>">
  	          <!-- <input name="don_gia[]" class="form-control" type="number" > -->
  	        </td>
  	        <td >
              <?php 
                if($tang[$i] === 'true'){
              ?>
                0
                <input name="thanh_tien[]" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="0">
              <?php    
                }else{
              ?>
                <?=number_format($thanh_tien[$i])?>
                <input name="thanh_tien[]" type="hidden" class="form-control" placeholder=""  readonly="readonly" value="<?=$thanh_tien[$i]?>">
              <?php 
                }
              ?>

  	        </td>
            <td>
              <?=($tang[$i] == 'true') ? '0' : $khuyen_mai[$i];?>
              <!-- <?=$khuyen_mai[$i]?> -->
              <?= ($khuyen_mai[$i]) < 101 ?'%':'đ'  ?>
              <input min="0" value="<?=($tang[$i] == 'true') ? '0' : $khuyen_mai[$i] ;?>"  name="khuyen_mai[]" class="form-control" type="hidden" readonly  />
            </td>
            <td>
              <input readonly onclick="return false" class="form-control"  type="checkbox" name="tang[]" 
                <?= ($tang[$i] === 'true') ? 'checked' :''; ?> 
                value="true"  
              />
              <?php 
                if($tang[$i] === 'false'){
              ?>
                <input class="tang_hidden" type="hidden" name="tang[]" value="false">
              <?php
                }
              ?>
            </td>
  	      </tr>
        <?php
      		}
      	?>
        <tr>
          <td colspan="3"><b>Tổng tiền</b></td>
           <td colspan="3"><b><?=$tongTien?></b></td>
        </tr>

      </tbody>
  	</table>
  	<div class="form-actions">
      <button name="saveView" class="btn btn-primary d-print-none">
        Save
      </button>
      <a data-toggle="modal" data-target="" class="btn btn-info d-print-none" id="back">
      	<!-- <i class="far fa-save"></i> -->
      	Back
      </a>
      <a class="btn btn-warning d-print-none" id="print" >
      	<!-- <i class="fa fa-print" ></i> -->
      	Print
    	</a>
    </div>
  </form>
</div>


<script>
  
    $('#printAdd #view_form').submit(function(e) {
      e.preventDefault(); 
      $('#wait').show();
      dataForm = $(this).serialize();
      url = 'controller/hoadons/save.php';
      // dataJson = JSON.stringify(dataForm);
      console.log(dataForm);
      
      $.ajax({
        url: url,
        type: 'POST',
        data: dataForm,
        success: function(data) {
            $('#wait').hide();
            // alert('Thêm mới hóa đơn thành công');
            location.reload();
        },
        error: function(data){
            $('#wait').hide();
        }               
      });
    });
  
</script>