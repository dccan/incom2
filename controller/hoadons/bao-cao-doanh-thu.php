<?php 
require_once('../DB_driver.php');
$db = new DB_driver();

$maKH = $_POST['ma_kh'];
$startDate = $_POST['start_date'];
$endDate = $_POST['end_date'];

if( $startDate >  $endDate ) {
	$tam = $endDate;
	$endDate = $startDate;
	$startDate = $tam;
}

if ( $maKH != "" && $startDate !="" && $endDate !=""){

	$hoaDon = $db->get_list("select * from hoadon where ngay_lap between  '$startDate' and '$endDate' and ma_kh = $maKH ");

	$sumTriGia = $db->get_list("select sum(tri_gia) as tri_gia from hoadon where ngay_lap between  '$startDate' and '$endDate' and ma_kh = $maKH ");

} else {

	$hoaDon = $db->get_list("select * from hoadon where ngay_lap between  '$startDate' and '$endDate' ");

	$sumTriGia = $db->get_list("select sum(tri_gia) as tri_gia from hoadon where ngay_lap between  '$startDate' and '$endDate' ");

}






?>


<div class="table-responsive">
  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
      <tr>
        <th>Số HD</th>
        <th>Ngày lập</th>
        <th>Mã khách hàng</th>
        <th>Trị giá</th>
        <th>Thanh toán</th>
        <th>Tình trạng</th>
        <th></th>
      </tr>
    </thead>
    <!-- <tfoot>
      <tr>
        <th>Số HD</th>
        <th>Ngày lập</th>
        <th>Mã khách hàng</th>
        <th>Trị giá</th>
        <th>Thanh toán</th>
        <th>Tình trạng</th>
        <th></th>
      </tr>
    </tfoot> -->
    <tbody>
      <?php 
        foreach ($hoaDon  as $key => $value) {
      ?>
        <tr>
          <td><?=$value['so_hd']?></td>
          <td><?=$value['ngay_lap']?></td>
          <td><?=$value['ma_kh']?></td>
          <td><?=$value['tri_gia']?></td>
          <td><?=$value['thanh_toan']?></td>
          <td><?=$value['tinh_trang']?></td>
          <th><a id="<?=$value['so_hd']?>" data-toggle="modal" data-target="#chitiet" ><i class="fa fa-info-circle" aria-hidden="true"></i></a></th>
        </tr>

      <?php 
        }
      ?>
      <tr>
      	<td colspan="3">
      		<b>Thành tiền</b>
      	</td>
      	<td colspan="3">
      		<b><?=$sumTriGia[0]['tri_gia']?> </b>
      	</td>
      </tr>
    </tbody>
  </table>
</div>