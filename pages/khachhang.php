<?php 
  session_start();
  require_once('../head.php');
?>

<?php 
  require_once('../nav.php');
?>


<?php 
  require_once('../controller/DB_driver.php');

  $db = new DB_driver();

  $sanPham = $db->get_list('select * from sanpham');

  
?>

<style>
table img {
  width: 100px;
}
  
</style>

<div id="wrapper">

    <!-- Sidebar -->
    <?php 
      require_once('../sidebar.php');
    ?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Sản Phẩm</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <!-- <i class="fas fa-table"></i> -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"  data-backdrop="static" data-keyboard="false" >
              <i class="fas fa-plus"></i> ADD
            </button>  
          </div>
          
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Tên sản phẩm</th>
                    <th>Hình</th>
                    <th>Mô tả</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Tên sản phẩm</th>
                    <th>Hình</th>
                    <th>Mô tả</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php 
                    foreach ($sanPham  as $key => $value) {
                  ?>
                    <tr>
                      <td><?=$value['ten_sp']?></td>
                      <td><img src="<?=$value['hinh']?>" /></td>
                      <td><?=$value['mo_ta']?></td>
                      <td><?=$value['so_luong']?></td>
                      <td><?=$value['don_gia']?></td>
                      <td> 
                        <button data-toggle="modal" data-target="#edit"  id="<?=$value['ma_sp']?>" class="edit btn btn-info"  >
                          <i class="fas fa-edit"></i> Sửa
                        </button> 
                        <button class="delete btn btn-danger" id="<?=$value['ma_sp']?>" >
                          <i class="fas fa-times"></i> Xóa
                        </button>
                      </td>
                    </tr>

                  <?php 
                    }
                  ?>
                  
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->
</div>
<!--end-main-container-part-->

  <div class="modal fade" id="add" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              <!-- <h5>Personal-info</h5> -->
            </div>
            <div class="widget-content nopadding">
              <form id="addFrom" action="/pages/sanpham.php" method="post" class="form-horizontal" enctype="multipart/form-data" >
                
                <!-- <div class="control-group">
                  <label class="control-label">ten_sp Name :</label>
                  <div class="controls">
                    <input name="ma_sp" type="hidden" class="form-control" 
                    value="null" 
                    >
                  </div>
                </div> -->

                <div class="control-group">
                  <label class="control-label">ten_sp Name :</label>
                  <div class="controls">
                    <input name="ten_sp" type="text" class="form-control" placeholder="First name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Hình:</label>
                  <div class="controls">
                    <input id="hinh" name="hinh" type="text" class="form-control" placeholder="First name">
                    <buton class="btn btn-info" id="uploadImg">Upload</buton>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">mo_ta Name :</label>
                  <div class="controls">
                    <input name="mo_ta" type="text" class="form-control" placeholder="Last name">
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">so_luong info :</label>
                  <div class="controls">
                    <input name="so_luong" type="number" class="form-control" placeholder="Company name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">don_gia info :</label>
                  <div class="controls">
                    <input name="don_gia" type="number" class="form-control" placeholder="Company name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">don_vi_tinh info :</label>
                  <div class="controls">
                    <input name="don_vi_tinh" type="text" class="form-control" placeholder="Company name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">chi_tiet info :</label>
                  <div class="controls">
                    <input name="chi_tiet" type="text" class="form-control" placeholder="Company name">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">ma_loai info :</label>
                  <div class="controls">
                    <input name="ma_loai" type="number" class="form-control" placeholder="Company name">
                  </div>
                </div>
                
                
                <div class="form-actions">
                  <button type="submit" name="addSubmit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="edit" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sửa sản phẩm</h4>
        </div>
        <div class="modal-body">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              <!-- <h5>Personal-info</h5> -->
            </div>
            <div class="widget-content nopadding">
              <form accept-charset="utf-8" id="editFrom" action="" method="post" class="form-horizontal" >
                
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



<?php 
  require_once('../footer.php');
?>

<script>
    $( document ).ready(function() {


      $('#addFrom').submit(function(e) {
        url = 'controller/sanphams/add.php';
        dataForm = $(this).serialize()
        e.preventDefault(); 
        $.ajax({
            url: url,
            type: 'POST',
            data: dataForm,
            success: function(msg) {
                alert('Thêm mới thành công');
                location.reload();
            }               
        });
      });

      $('.edit').click(function(e) {
        id = $(this).attr('id');
        url = 'controller/sanphams/edit.php?id='+id;
        e.preventDefault(); 
        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                $('#editFrom').html(data);
            }               
        });
      });

      $('.delete').click(function(e) {

        var confirmation = confirm("Bạn có muốn xóa sản phẩm này");
        if (confirmation) {
          id = $(this).attr('id');
          url = 'controller/sanphams/delete.php?id='+id;
          e.preventDefault(); 
          $.ajax({
              url: url,
              type: 'GET',
              success: function(msg) {
                  location.reload();
              }               
          });
        }
          
      });  

      $('#editFrom').submit(function(e) {
        url = 'controller/sanphams/update.php';
        dataForm = $(this).serialize()
        e.preventDefault(); 
        $.ajax({
            url: url,
            type: 'POST',
            data: dataForm,
            success: function(msg) {
                alert('Cập nhật thành công');
                location.reload();
            }               
        });
      });  

      var btnUpload = document.getElementById( 'uploadImg' );
      btnUpload.onclick = function() {
        selectFileWithCKFinder( 'hinh' );
      };

      function selectFileWithCKFinder( elementId ) {
        CKFinder.modal( {
          chooseFiles: true,
          width: 800,
          height: 600,
          onInit: function( finder ) {
            finder.on( 'files:choose', function( evt ) {
              var file = evt.data.files.first();
              var output = document.getElementById( elementId );
              output.value = file.getUrl();
            } );

            finder.on( 'file:choose:resizedImage', function( evt ) {
              var output = document.getElementById( elementId );
              output.value = evt.data.resizedUrl;
            } );
          }
        } );
      }
      

    });
</script>
