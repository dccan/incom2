<?php 
  session_start();
  require_once('../head.php');
?>

<?php 
  require_once('../nav.php');
?>


<?php 
  require_once('../controller/DB_driver.php');

  $db = new DB_driver();

  $hoaDon = $db->get_list('select hoadon.*,khachhang.ho_ten as ho_ten from hoadon,khachhang where khachhang.ma_kh = hoadon.ma_kh');

  $khachHang = $db->get_list('select ma_kh,ho_ten from khachhang  ');

  $tenSP = $db->get_list("select ma_sp,ten_sp,don_gia from sanpham ");
  $now = date("Y-m-d");
?>

<style>
table img {
  width: 100px;
}
  
</style>

<div id="wrapper">

    <!-- Sidebar -->
    <?php 
      require_once('../sidebar.php');
    ?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Hóa đơn</li>
        </ol>

        <!-- DataTables Example  data-backdrop="static" data-keyboard="false"-->
        <div class="card mb-3">
          <div class="card-header">
            <!-- <i class="fas fa-table"></i> -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add"   >
              <i class="fas fa-plus"></i> ADD
            </button>  
            <form style="display: inline" class="col-8" id="report_hd" action="" method="get" >
              <select class="selectpicker " name="ma_kh" data-show-subtext="true" data-live-search="true">
                <option value="">Chọn khách hàng</option>
                <?php
                  foreach ($khachHang as $key => $value) {
                ?>
                  <option value="<?=$value['ma_kh']?>">
                    <?=$value['ho_ten']?>
                  </option>
                <?php } ?>
              </select>
              <input type="date"  name="start_date" placeholder="Ngày bắt đầu" required >
              <input type="date"  name="end_date" placeholder="Ngày kết thúc" required >
              
              <button class="btn btn-success">Search</button>
            </form>
            
          </div>
          
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Số HD</th>
                    <th>Ngày lập</th>
                    <th>Khách hàng</th>
                    <th>Trị giá</th>
                    <th>Thanh toán</th>
                    <th>Tình trạng</th>
                    <th></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Số HD</th>
                    <th>Ngày lập</th>
                    <th>Khách hàng</th>
                    <th>Trị giá</th>
                    <th>Thanh toán</th>
                    <th>Tình trạng</th>
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php 
                    foreach ($hoaDon  as $key => $value) {
                      $date = new DateTime($value['ngay_lap']);
                  ?>
                    <tr>
                      <td><?=$value['so_hd']?></td>
                      <td><?=date_format($date, 'd-m-Y');?></td>
                      <td><?=$value['ho_ten']?></td>

                      <td><?=$value['tri_gia']?></td>
                      <td><?=$value['thanh_toan']?></td>
                      <td><?=$value['tinh_trang']?></td>
                      <th><a id="<?=$value['so_hd']?>" data-toggle="modal" data-target="#chitiet" ><i class="fa fa-info-circle" aria-hidden="true"></i></a>
                      <a class="edit" id="<?=$value['so_hd']?>" data-toggle="modal" data-target="#edit" ><i class="fa fa-edit" aria-hidden="true"></i></a>
                      </th>
                    </tr>

                  <?php 
                    }
                  ?>
                  
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->
</div>
<!--end-main-container-part-->

<!--add hoa don-->
  <div class="modal fade" id="add" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content "  >
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Hóa Đơn</h4>
        </div>
        <div class="modal-body">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              <!-- <h5>Personal-info</h5> -->
            </div>
            <div id="printAdd"  class="widget-content nopadding printAdd">



              <form id="addFrom" action="" method="post" class="form-horizontal" enctype="multipart/form-data" >
              
                <div class="col-md-12 data_add d-print-none">  

                  <div class="form-group">
                    <div class="form-row">
                      <div class="control-group col-md-6">
                        <label class="control-label">Số hóa đơn: </label>
                        <div class="controls">
                          <input name="so_hd" type="text" class="form-control" placeholder="Số hóa đơn auto"  readonly="readonly">
                        </div>
                      </div>

                      <div class="control-group col-md-6">
                        <label class="control-label">Ngày lập: </label>
                        <div class="controls">

                          <input name="ngay_lap" value="<?=$now?>" type="date" class="form-control" placeholder="Ngày lập" required>
                        </div>
                      </div>
                      
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="form-row">
                        <div class="control-group col-6">
                          <label class="control-label">Mã khách hàng: </label>
                          <div class="controls ">
                            
                            <!-- <input name="ma_kh" type="text" class="form-control" placeholder="Mã khách hàng" required> -->
                            <select class="selectpicker " class="form-control" name="ma_kh" data-show-subtext="true" data-live-search="true" required>
                              <option value="">Chọn khách hàng</option>
                              <?php
                                foreach ($khachHang as $key => $value) {
                              ?>
                                <option value="<?=$value['ma_kh']?>">
                                  <?=$value['ho_ten']?>
                                </option>
                              <?php } ?>
                            </select>

                          </div>
                        </div>

                        <div class="control-group col-6">
                          <label class="control-label">Trị giá: </label>
                          <div class="controls">
                            <input name="tri_gia" type="text" class="form-control" placeholder="Trị giá auto" readonly="">

                          </div>
                        </div>
                    </div>
                  </div>


                  <div class="form-group">
                    <div class="form-row">
                      <div class="control-group col-6">
                        <label class="control-label">Thanh toán: </label>
                        <div class="controls">
                          <input name="thanh_toan" min="1" value="0" type="text" class="form-control" placeholder="Thanh toán" >
                        </div>
                      </div>

                      <div class="control-group col-6">
                        <label class="control-label">Tình trạng: </label>
                        <div class="controls">
                          <input name="tinh_trang" min="1" value="0" type="text" class="form-control" placeholder="Tình trạng" >
                        </div>
                      </div>
                    </div>
                  </div>
                
                  <div class="card-header">
                    <i class="fas fa-table"></i>
                    Chi Tiết Hóa Đơn
                  </div>
                  <table class="recore col-12">
                      <thead>
                        <tr>
                          <!-- <th>Số hóa đơn</th> -->
                          <th>Mã sản phẩm</th>
                          <th>Số lượng</th>
                          <th>Đơn giá</th>
                          <th>Thành tiền</th>
                          <th>Khuyến mãi</th>
                          <th>Tặng</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>

                      <tr class="tr-sp">
                       <!--  <td>
                          <input name="so_hd[]" class="form-control" type="number">
                        </td> -->
                        <td class="td-masp">

                          <!-- <input class="ma_sp" name="ma_sp[]" class="form-control" type="text" required  ></input> -->
                           <select class="selectpicker" name="ma_sp[]" data-show-subtext="true" data-live-search="true" required>
                            <option value="">Chọn sản phẩm</option>
                            <?php
                              foreach ($tenSP as $key => $value) {
                            ?>
                              <option gia="<?=$value['don_gia']?>" value="<?=$value['ma_sp']?>">
                                <?=$value['ten_sp']?>
                              </option>
                            <?php } ?>
                          </select>  
                        </td>
                        <td>
                          <input  name="so_luong[]" min='1' value="1" class="form-control so_luong" type="number" required />
                        </td>
                        <td>
                          <input name="don_gia[]" readonly class="form-control don_gia" type="number" required />
                        </td>
                        <td class="td_thanh_tien"> 
                          <input  name="thanh_tien[]" class="form-control thanh_tien" type="number" readonly required />
                        </td>
                        <td>
                          <input  name="khuyen_mai[]" min="0" value="0" class="form-control" type="number"   />
                          
                        </td>
                        <td>
                          <input class="tang form-control" type="checkbox" name="tang[]" value="true">
                          <input class="tang_hidden" type="hidden" name="tang[]" value="false">
                          
                        </td>
                        <td>
                          <a id="add_recore" class="btn btn-success"><i class="fas fa-plus"></i></a>
                        </td>
                      </tr>
                      </tbody>
                    </table>

                  <div class="form-actions">
                    <button  name="addSubmit" class="btn btn-primary">
                      View
                    </button>
                    <!-- <a  id="view" class="btn btn-secondary">
                      View
                    </a> -->
                    <!-- <a class="btn btn-warning" id="print" >Print</a> -->
                  </div>
                 </div> 
              </form>

              <div>
                
              </div>
            </div>
          </div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
  </div>
<!--end add hoa don -->  


<!--edit -->
  <div class="modal fade" id="edit" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content "  >
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Sửa Hóa Đơn</h4>
        </div>
        <div class="modal-body">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              
            </div>
            <div id="edit_hd"  class="widget-content nopadding ">
                            
            </div>
          </div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
  </div>
<!--end edit hoa don -->  

<?php 
  require_once('../footer.php');
?>

<script>
    $( document ).ready(function() {

      recore = 1;
      $('.selectpicker').selectpicker();
      $(".recore").on("click", ".rm_recore", function (event) {
        
        // Delete recore
        $(this).parent().parent('.copy').remove();
      });
      
      $("#addFrom .tang" ).change(function() {
        positin = $( this );
        if(positin.is( ":checked" ) == true) {
          positin.parent().find('.tang_hidden').prop('disabled', true);
        }else {
          positin.parent().find('.tang_hidden').prop('disabled', false);
        }
      });
      
      $('#add_recore').click(function(e) {
        // Add recore
        $('#wait').show();
        url = 'controller/hoadons/recore.php';
        e.preventDefault(); 
        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                $('#wait').hide();
                // location.reload();
                $(".recore").append(data);
            },
            error: function(data){
                $('#wait').hide();
            }               
        });
      });
      
      $('#report_hd').submit(function(e) {

        $('#wait').show();
        dataForm = $('#report_hd').serialize();
        url = 'controller/hoadons/bao-cao-doanh-thu.php';

        // dataJson = JSON.stringify(dataForm);
        e.preventDefault(); 
        $.ajax({
            url: url,
            type: 'POST',
            data: dataForm,
            success: function(data) {
                $('#wait').hide();
                // $('#addFrom').html(data);
                $('.card-body').html(data);
                // location.reload();
            },
            error: function(data){
                $('#wait').hide();
            }               
        });
      });

      $('#addFrom').submit(function(e) {

        $('#wait').show();
        dataForm = $('#addFrom').serializeArray();
        url = 'controller/hoadons/view.php';

        // dataJson = JSON.stringify(dataForm);
        console.log(dataForm);
        e.preventDefault(); 
        $.ajax({
            url: url,
            type: 'POST',
            data: dataForm,
            success: function(data) {
                $('#wait').hide();
                // $('#addFrom').html(data);
                $('.data_add').hide();
                $('#printAdd').append(data);
                // location.reload();
            },
            error: function(data){
                $('#wait').hide();
            }               
        });
      });
      
      $(".don_gia").keyup(function(){ 
        positin = $(this);
        so_luong = $('.so_luong').val();
        don_gia = $('.don_gia').val();
        thanh_tien= so_luong * don_gia;
        $(positin).parent().parent().find('.td_thanh_tien .thanh_tien').val(thanh_tien);
      });

      $(".so_luong").keyup(function(){
        positin = $(this);
        so_luong = positin.val(); ;
        don_gia = $('.don_gia').val();
        thanh_tien= so_luong * don_gia;
        $(positin).parent().parent().find('.td_thanh_tien .thanh_tien').val(thanh_tien);
      
      });

      $(".so_luong").bind('keyup mouseup', function () {
        positin = $(this);
        so_luong = positin.val(); ;
        don_gia = $('.don_gia').val();
        thanh_tien= so_luong * don_gia;
        $(positin).parent().parent().find('.td_thanh_tien .thanh_tien').val(thanh_tien);           
      });

      $(".ma_sp").keyup(function(){
        ten_sp = $(this).val();
        positin = $(this);
        
        if(ten_sp.length > 0){
            url = 'controller/hoadons/search-name-sp.php?ten_sp='+ten_sp;
           $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                $('#wait').hide();
                $('.data-sp').remove();

                $(positin).parent('.td-masp').append(data);
                // location.reload();
            },
            error: function(data){
                $('#wait').hide();
            }               
          });
        }else {
          $('.data-sp').remove();
        }
       
      });

      //data-sp

      
      $(".wrap_masp").on("click", ".data-sp a", function (event) {
        id = $(this).attr('id');
        postion = $(this);
        $(this).parent().parent().parent('.wrap_masp').val(id);
        // $('#view_form').remove();
        
      });

      $( ".td-masp select" ).change(function() {
        a = $(this).attr('gia');
        var gia = $('option:selected', this).attr('gia');
        
        $('.tr-sp .don_gia').val(gia);
        thanh_tienien = gia *1;
        $('.tr-sp .thanh_tien').val(gia);

      });


      $(".td-masp").on("click", ".data-sp a", function (event) {
        id = $(this).attr('id');
        alert('adfasaf');
        postion = $(this);
        postion.parent().parent().parent().find('ma_sp').val(id);
        // $('#view_form').remove();
        
      });

      $('#print').click(function(e) {
        printDiv('printAdd');
        // window.print();
      });
      
      $(".printAdd").on("click", "#back", function (event) {
        
        $('#view_form').remove();
        $('.data_add').show();
      });
      $(".printAdd").on("click", "#print", function (event) {
        printDiv('printAdd');
      });

      function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        w=window.open();
        // w.document.write('<html><head><title>Print it!</title><link rel="stylesheet" type="text/css" href="http://localhost:8888/css/sb-admin.css"></head><body>');
        // w.document.write('</head><body >');
        // w.document.write(printContents);
        // w.document.write('</body></html>');
        // w.print();
        // w.close();

        jQuery.get('css/print.css', function (data) {
            w.document.write('<html><head><style>');
            w.document.write(data);
            w.document.write('</style></head><body>');
            w.document.write(printContents);
            w.document.write('</body></html>');
            w.print();
            w.close();
        });


      }

      // Sua hoa don

      $('.edit').click(function(e) {
        e.preventDefault(); 
        id = $(this).attr('id');
        $('#wait').show();
        url = 'controller/hoadons/edit.php?so_hd='+id;
        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                $('#wait').hide();
                // location.reload();
                $("#edit_hd").html(data);
            },
            error: function(data){
                $('#wait').hide();
            }               
        });
        
      });


      

    });
</script>
