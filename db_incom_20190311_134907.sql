-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE TABLE "chitiethoadon" ----------------------------
-- CREATE TABLE "chitiethoadon" --------------------------------
CREATE TABLE `chitiethoadon` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`so_hd` Int( 11 ) NOT NULL,
	`ma_sp` Int( 11 ) NOT NULL,
	`so_luong` Int( 255 ) NOT NULL,
	`don_gia` Float( 12, 0 ) NOT NULL,
	`thanh_tien` Float( 12, 0 ) NOT NULL,
	`khuyen_mai` Int( 255 ) NOT NULL,
	`tang` VarChar( 5 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "hoadon" -----------------------------------
-- CREATE TABLE "hoadon" ---------------------------------------
CREATE TABLE `hoadon` ( 
	`so_hd` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`ngay_lap` Date NOT NULL,
	`ma_kh` Int( 11 ) NOT NULL,
	`tri_gia` Float( 12, 0 ) NOT NULL,
	`thanh_toan` Float( 12, 0 ) NOT NULL,
	`tinh_trang` TinyInt( 255 ) NOT NULL,
	PRIMARY KEY ( `so_hd` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "khachhang" --------------------------------
-- CREATE TABLE "khachhang" ------------------------------------
CREATE TABLE `khachhang` ( 
	`ma_kh` Int( 11 ) NOT NULL,
	`ho_ten` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`dai_chi` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`dien_thoai` VarChar( 11 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`email` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`mat_khau` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY ( `ma_kh` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "sanpham" ----------------------------------
-- CREATE TABLE "sanpham" --------------------------------------
CREATE TABLE `sanpham` ( 
	`ma_sp` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`ten_sp` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`hinh` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`mo_ta` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`so_luong` Int( 255 ) NOT NULL,
	`don_gia` Decimal( 10, 0 ) NOT NULL,
	`don_vi_tinh` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`chi_tiet` VarChar( 500 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`ma_loai` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `ma_sp` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 12;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- Dump data of "chitiethoadon" ----------------------------
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '3', '6', '5', '2', '60000', '120000', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '4', '6', '5', '2', '60000', '120000', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '5', '6', '10', '2', '60000', '120000', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '6', '6', '5', '1', '60000', '60000', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '7', '7', '9', '1', '60000', '60000', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '8', '7', '10', '1', '60000', '60000', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '9', '7', '10', '1', '60000', '60000', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '10', '8', '8', '1', '123', '123', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '11', '8', '10', '1', '60000', '0', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '12', '8', '5', '1', '60000', '0', '0', 'true' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '13', '8', '11', '1', '60000', '60000', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '14', '9', '5', '1', '60000', '60000', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '15', '9', '8', '1', '123', '123', '0', 'false' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '16', '9', '9', '1', '60000', '0', '0', 'true' );
INSERT INTO `chitiethoadon`(`id`,`so_hd`,`ma_sp`,`so_luong`,`don_gia`,`thanh_tien`,`khuyen_mai`,`tang`) VALUES ( '17', '9', '10', '1', '60000', '0', '0', 'true' );
-- ---------------------------------------------------------


-- Dump data of "hoadon" -----------------------------------
INSERT INTO `hoadon`(`so_hd`,`ngay_lap`,`ma_kh`,`tri_gia`,`thanh_toan`,`tinh_trang`) VALUES ( '6', '2019-03-11', '1', '420000', '1', '1' );
INSERT INTO `hoadon`(`so_hd`,`ngay_lap`,`ma_kh`,`tri_gia`,`thanh_toan`,`tinh_trang`) VALUES ( '7', '2019-03-11', '1', '180000', '0', '0' );
INSERT INTO `hoadon`(`so_hd`,`ngay_lap`,`ma_kh`,`tri_gia`,`thanh_toan`,`tinh_trang`) VALUES ( '8', '2019-03-11', '1', '60123', '0', '0' );
INSERT INTO `hoadon`(`so_hd`,`ngay_lap`,`ma_kh`,`tri_gia`,`thanh_toan`,`tinh_trang`) VALUES ( '9', '2019-03-11', '1', '60123', '0', '0' );
-- ---------------------------------------------------------


-- Dump data of "khachhang" --------------------------------
INSERT INTO `khachhang`(`ma_kh`,`ho_ten`,`dai_chi`,`dien_thoai`,`email`,`mat_khau`) VALUES ( '1', 'Teo', 'hcm', '123456', 'teo@gmail.com', '123456' );
-- ---------------------------------------------------------


-- Dump data of "sanpham" ----------------------------------
INSERT INTO `sanpham`(`ma_sp`,`ten_sp`,`hinh`,`mo_ta`,`so_luong`,`don_gia`,`don_vi_tinh`,`chi_tiet`,`ma_loai`) VALUES ( '5', 'Sản phẩm 1', '/ckfinder/userfiles/images/tra-olong.jpg', 'Tra ngon', '12', '60000', 'Gói', 'Đang cập nhật', '1' );
INSERT INTO `sanpham`(`ma_sp`,`ten_sp`,`hinh`,`mo_ta`,`so_luong`,`don_gia`,`don_vi_tinh`,`chi_tiet`,`ma_loai`) VALUES ( '8', 'Sản phẩm 5', '/ckfinder/userfiles/images/tra-olong.jpg', 'Tra ngon23', '123', '123', 'Gói23', 'đang cập nhật', '123' );
INSERT INTO `sanpham`(`ma_sp`,`ten_sp`,`hinh`,`mo_ta`,`so_luong`,`don_gia`,`don_vi_tinh`,`chi_tiet`,`ma_loai`) VALUES ( '9', 'Sản phẩm 3', '/ckfinder/userfiles/images/tra-olong.jpg', 'Tra ngon', '1', '60000', 'Gói', 'đang cập nhật', '1' );
INSERT INTO `sanpham`(`ma_sp`,`ten_sp`,`hinh`,`mo_ta`,`so_luong`,`don_gia`,`don_vi_tinh`,`chi_tiet`,`ma_loai`) VALUES ( '10', 'Sản phẩm 4', '/ckfinder/userfiles/images/tra-olong.jpg', 'trà ô long', '12', '60000', 'Gói', 'Đang cập nhật', '1' );
INSERT INTO `sanpham`(`ma_sp`,`ten_sp`,`hinh`,`mo_ta`,`so_luong`,`don_gia`,`don_vi_tinh`,`chi_tiet`,`ma_loai`) VALUES ( '11', 'Sản phẩm 6', '/ckfinder/userfiles/images/tra-olong.jpg', 'Trà ô long	    ', '112', '60000', 'Gói', 'Trà ô long Thái Nguyên	    ', '1' );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


